//
//  Info.swift
//  Aglaya PLAY
//
//  Created by Gian on 06/11/2019.
//  Copyright © 2019 Giannino Clemente. All rights reserved.
//

import UIKit

class Info: UIViewController {

    @IBOutlet weak var AglayaBut: UIButton!
    @IBOutlet weak var FSMCVBut: UIButton!
    @IBOutlet weak var ICIEBut: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func AglayaButPressed(_ sender: Any) {
        if let url = URL(string: "https://www.aglaya.org") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBAction func FSMCVButPressed(_ sender: Any) {
        if let url = URL(string: "https://fsmcv.org") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
    }
    @IBAction func ICIEButPressed(_ sender: Any) {
        if let url = URL(string: "https://www.uv.es/uvweb/institut-creativitat-innovacions-educatives/ca/institut-universitari-creativitat-innovacions-educatives-icie-1285894011319.html") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
