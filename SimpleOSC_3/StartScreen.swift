//
//  StartScreen.swift
//  SimpleOSC_3
//
//  Created by Gian on 23/09/2019.
//  Copyright © 2019 Giannino Clemente. All rights reserved.
//

import UIKit
import SwiftOSC

var INport = 8009
var server = OSCServer(address: "", port: INport)
var OUTport = 7999
var client = OSCClient(address: "255.255.255.255", port: OUTport)
var DeviceID = 0
var SwitchOn = false
var TextIP = "255.255.255.255"

func updateipchanges() {
    TextIP = client.address
    //CustomIPTextField.text = client.address
    print ("IP is \(client.address)")
}

class OSCHandler: OSCServerDelegate {

    func didReceive(_ message: OSCMessage){
        if let string = message.arguments[0] as? String {
            print("Received String \(string)")
            client.address = (string)
            updateipchanges()
        } else {
            print(message)
        }
    }
    
    
}

class StartScreen: UIViewController {

    @IBOutlet weak var CustomIPTextField: UITextField!
    @IBOutlet weak var Switch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        CustomIPTextField.delegate = self
        Switch.isOn = SwitchOn
        if SwitchOn == true {CustomIPTextField.isHidden = false}
        CustomIPTextField.text = client.address
        print ("IP is \(client.address)")
        print ("Port is \(client.port)")
        
        server.start()
        server.delegate = OSCHandler()
        
    }
    
   
    @IBAction func SwitchChanged(_ sender: Any) {
        if Switch.isOn == true {
            CustomIPTextField.text = TextIP
            CustomIPTextField.isHidden = false
            client.address = String(CustomIPTextField.text ?? "255.255.255.255")
            SwitchOn = true
        }else{
            CustomIPTextField.isHidden = true
            TextIP = CustomIPTextField.text!
            client.address = "255.255.255.255"
            SwitchOn = false
        }
        print ("IP is \(client.address)")
    }
    
    @IBAction func CustomIPTextFieldEditEnd(_ sender: Any) {
        client.address = String(CustomIPTextField.text ?? "255.255.255.255")
        print ("IP set to \(client.address)")

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        CustomIPTextField.resignFirstResponder()
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension StartScreen: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
